<?php

require_once 'model.php';

class Recipe{
	public $id;
	public $title;
	public $ingredient0;
	public $ingredient1;
	public $ingredient2;
	public $instructions;
	
	function __construct($id, $title, $ingredient0, $ingredient1, $ingredient2, $instructions) {
		$this->id = $id;
		$this->title = $title;
		$this->ingredient0 = $ingredient0;
		$this->ingredient1 = $ingredient1;
		$this->ingredient2 = $ingredient2;
		$this->instructions = $instructions;
	}
}

class RecipeModel extends Model{
	function findAll() {
		$conn = Database::get_connection();
		$query = "SELECT * from recipe";
		$res = $conn->query($query);
		
		while ($row = $res->fetch_assoc()) {
			$results[] = new Recipe(
			$row['id'],
            $row['title'],
            $row['ingredient0'],
			$row['ingredient1'],
			$row['ingredient2'],
			$row['instructions']
            );
		}
		return $results;
	}
	
	function insertRecipe() {
		$conn = Database::get_connection();
		$query = $conn->prepare("INSERT INTO recipe (title, ingredient0, ingredient1, ingredient2, instructions) VALUES (?, ?, ?, ?, ?)");
		$query->bind_param('sssss', $_POST['title'], $_POST['ingredient0'], $_POST['ingredient1'], $_POST['ingredient2'], $_POST['instructions']);
		$query->execute();
	}
}

?>