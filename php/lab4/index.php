<?php
function render($page) {
    ob_start();
    include $page . '.php';
    $content = ob_get_clean();
    include 'layout.php';
}

//display form
if ($_SERVER['REQUEST_METHOD'] == "GET" && (isset($_GET['action']) && $_GET['action']=="insert")) {
	render('form');
}
//display thank you
elseif ($_SERVER['REQUEST_METHOD'] == "POST" && (isset($_GET['action']) && $_GET['action']=="insert")) {
	render('thanks');
}
//display list
else {
	render('list');
}
?>