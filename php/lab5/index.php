<?php

require_once 'recipemodel.php';
require_once 'view.php';

//display form
if ($_SERVER['REQUEST_METHOD'] == "GET" && (isset($_GET['action']) && $_GET['action']=="insert")) {
	$pageName='form';
	$pageData=NULL;
	$viewObject = new View($pageName, $pageData);
	$viewObject->render();
}
//display thank you
elseif ($_SERVER['REQUEST_METHOD'] == "POST" && (isset($_GET['action']) && $_GET['action']=="insert")) {
	$pageName='thanks';
	$pageData=NULL;
	$viewObject = new View($pageName, $pageData);
	$viewObject->render();
}
//display list
else {
	$pageName='list';
	$object = new recipeModel();
	$pageData=$object->findAll();
	$viewObject = new View($pageName, $pageData);
	$viewObject->render();
}
?>

